import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-ghe-item',
  templateUrl: './ghe-item.component.html',
  styleUrls: ['./ghe-item.component.scss']
})
export class GheItemComponent implements OnInit {
  status:boolean = false;
  @Output() emitStatus = new EventEmitter(); 
  @Input() item!:Ghe;

  constructor() { }
  datGhe(){
    if(this.status){
      this.status=false;
    }else{
      this.status = true;
    }
    this.emitStatus.emit(this.status);
  }
  ngOnInit(): void {
  }

}

interface Ghe {
  SoGhe:number,
  TenGhe:string,
  Gia:number,
  TrangThai:boolean
}