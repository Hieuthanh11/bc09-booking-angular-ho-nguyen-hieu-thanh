import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GheItemComponent } from './ghe-item/ghe-item.component';
import { ListGheComponent } from './list-ghe/list-ghe.component';
import { EditListGheComponent } from './edit-list-ghe/edit-list-ghe.component';
import { BookingTicketComponent } from './booking-ticket.component';



@NgModule({
  declarations: [
    GheItemComponent,
    ListGheComponent,
    EditListGheComponent,
    BookingTicketComponent
  ],
  imports: [
    CommonModule
  ],
  exports :[
    BookingTicketComponent
  ],
})
export class BookingTicketModule { }
