import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-booking-ticket',
  templateUrl: './booking-ticket.component.html',
  styleUrls: ['./booking-ticket.component.scss']
})
export class BookingTicketComponent implements OnInit {
  arrItem:Ghe[]=[];
  count:number = 0

  constructor() {}
  ngOnInit(): void {
  }
  getValue(value:number){  
    // this.arrValue.push(value);
    this.count = value;
  }

  getGhe(item:Ghe[]){
    this.arrItem = item;
    // console.log(this.arrItem);
    
  }

}

interface Ghe {
  SoGhe:number,
  TenGhe:string,
  Gia:number,
  TrangThai:boolean
}