import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-edit-list-ghe',
  templateUrl: './edit-list-ghe.component.html',
  styleUrls: ['./edit-list-ghe.component.scss'],
})
export class EditListGheComponent implements OnInit {
  result!: number;
  constructor() {}
  @Input() value!: number;
  @Input() item!: Ghe[];

  ngOnInit(): void {}
  ngDoCheck() {
    console.log('run');

    this.result = this.item.reduce((total, obj) => {
      return (total += obj.Gia);
    }, 0);
  }
  huyGhe(value: Ghe) {
    for (let index in this.item) {
      if (this.item[index].SoGhe === value.SoGhe) {
        this.item.splice(parseInt(index), 1);
      }
    }
  }
}
interface Ghe {
  SoGhe: number;
  TenGhe: string;
  Gia: number;
  TrangThai: boolean;
}
