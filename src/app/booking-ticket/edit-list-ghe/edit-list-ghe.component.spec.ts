import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditListGheComponent } from './edit-list-ghe.component';

describe('EditListGheComponent', () => {
  let component: EditListGheComponent;
  let fixture: ComponentFixture<EditListGheComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditListGheComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditListGheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
